import './App.css';
import ParentComponent from './components/Parent';

function App() {
  return (
    <div>
     <ParentComponent/>
    </div>
  );
}

export default App;
