import { Component } from "react";
import DisplayComponent from "./Display";

class ParentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            display: false


        }
    }
    onBtnCreateComponentClick = () => {
        if (this.state.display === false) {
            this.setState({
                display: true

            })
        }else{
            this.setState({
                display: false

            })
        }

    }
    render() {
        return (
            <div>
                <button onClick={this.onBtnCreateComponentClick} >{this.state.display === false ? "Create Component" : "Destroy Component"}</button>
                <div>{this.state.display ? <DisplayComponent display={this.state.display} /> : null}</div>
            </div>
        )
    }
}
export default ParentComponent