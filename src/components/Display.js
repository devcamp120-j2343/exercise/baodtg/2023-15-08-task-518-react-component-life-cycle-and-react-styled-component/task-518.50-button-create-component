import React, { Component } from "react";

class DisplayComponent extends Component {
    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }
    render() {
        return (
            <div>
                <h1>I exist !</h1>
            </div>
        )
    }
}
export default DisplayComponent